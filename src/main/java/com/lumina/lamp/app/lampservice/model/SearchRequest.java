package com.lumina.lamp.app.lampservice.model;

public class SearchRequest {

	private JournalDetails journalId[];
	private String issueId;
	private String advertId;
	private Integer year;
	private String fileName;
	
	public SearchRequest()
	{
		
	}
	
	public JournalDetails[] getJournalId() {
		return journalId;
	}
	public void setJournalId(JournalDetails[] journalId) {
		this.journalId = journalId;
	}
	public String getIssueId() {
		return issueId;
	}
	public void setIssueId(String issueId) {
		this.issueId = issueId;
	}
	public String getAdvertId() {
		return advertId;
	}
	public void setAdvertId(String advertId) {
		this.advertId = advertId;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
	
}
