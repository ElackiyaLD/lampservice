package com.lumina.lamp.app.lampservice.util;

import java.io.File;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import com.lumina.lamp.app.lampservice.model.AdManagementDetails;

public class LampUtils {
	private String filePath="D:\\server\\tomcat8\\webapps\\ROOT\\stockage\\formattertypes\\advert\\APA\\";
	
//	@Value("${files.path}")
//    private String filePath;
	
	public String generateLDLFileFromClientFile(AdManagementDetails adManagementDetails,String dbFileName,String dbJournalName,boolean isEdit) throws Exception
	{
	

		File clientFile = new File(filePath+adManagementDetails.getClientFileName());
		File journalDir = new File(filePath+adManagementDetails.getJournalid());
		if(isEdit && !clientFile.exists())
		{
			clientFile = new File (filePath+dbJournalName+"\\"+dbFileName);
		}
		
		//Make Journal Folder if not exist
		if(!journalDir.isDirectory())
		{
			journalDir.mkdir();
		}
		
		File journalFile =  new File(journalDir+"\\"+clientFile.getName());
		String extension = FilenameUtils.getExtension(clientFile.getName());
		if(journalFile.exists())
		{
			File backupDir = new File(journalDir+"\\backup");
			if(!backupDir.isDirectory())
			{
				backupDir.mkdir();
			}
			String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
			String backupFileName = FilenameUtils.getBaseName(clientFile.getName())+"_"+timeStamp+"."+extension;
			
			FileUtils.copyFile(journalFile, new File(backupDir+"\\"+backupFileName));
		}
		
		//Move File to Journal Folder
		if(!dbJournalName.equalsIgnoreCase(adManagementDetails.getJournalid()))
		{
		FileUtils.copyFile(clientFile, journalFile);
		FileUtils.forceDelete(clientFile);
		}
		//Generate and copy LDL file
		String advertTitle = adManagementDetails.getAdvertTitle();
		advertTitle = advertTitle.replaceAll(" ", "_");
		
		String advertSize = adManagementDetails.getAdvertSize();
		advertSize = advertSize.replaceAll(" ", "_");
		advertSize = advertSize.replaceAll("/", "_");
		
		String ldlFileName = adManagementDetails.getAdvertId()+"_"+advertTitle
		+"_"+adManagementDetails.getColor_mode()+"_"+advertSize+"."+extension;
		
		File ldlFile = new File(journalDir+"\\"+ldlFileName);
		
		FileUtils.copyFile(journalFile, ldlFile);

		return ldlFile.getName();		
				
	}

	/***
	 * 
	 * @param filename
	 * @return
	 */
	public Resource fetchResource(String filename) {
        try {
        	System.out.println("File Path :"+filePath);
        	
            Path file = Paths.get(filePath)
                             .resolve(filename);
            System.out.println("File :"+file);
            Resource resource = new UrlResource(file.toUri());
             
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }
}
