package com.lumina.lamp.app.lampservice.controller;




import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lumina.lamp.app.lampservice.model.AdManagementDetails;
import com.lumina.lamp.app.lampservice.model.AdvertConfig;
import com.lumina.lamp.app.lampservice.model.JournalDetails;
import com.lumina.lamp.app.lampservice.model.JournalInfo;
import com.lumina.lamp.app.lampservice.model.SearchRequest;
import com.lumina.lamp.app.lampservice.service.AdmanagementService;
import com.lumina.lamp.app.lampservice.util.LampUtils;

@RestController
public class AdManagementController {

	@Autowired
	private AdmanagementService service;
	
	@PostMapping("/insert")
	@CrossOrigin(origins = "*")
	public AdManagementDetails saveAdManagementDetails(@RequestBody AdManagementDetails adManagementDetails) throws Exception 
	{
		AdManagementDetails responeAdManagementDetail = null;
		LampUtils lampUtils = new LampUtils();
		try {
		if(adManagementDetails !=null)
		{
			
			adManagementDetails.setSubmittedDate(new Date());
			adManagementDetails.setCreatedDate(new Date());
			adManagementDetails.setModifiedDate(new Date());
			adManagementDetails.setAdvertId(adManagementDetails.getAdvertId().trim());
			adManagementDetails.setAdvertTitle(adManagementDetails.getAdvertTitle().trim());
			adManagementDetails.setIssueId(adManagementDetails.getIssueId().trim());
			adManagementDetails.setActiveStatus("Active");
			
			String journalId = adManagementDetails.getJournalName().split(" - ")[0];
			adManagementDetails.setJournalid(journalId);
			
			String ldlFileName = lampUtils.generateLDLFileFromClientFile(adManagementDetails,"","",false);
			adManagementDetails.setLdlFileName(ldlFileName);
			
			responeAdManagementDetail = service.saveAdDetails(adManagementDetails);
		}
		}
        catch(Exception e)
		{
        	e.printStackTrace();
        	System.out.print("responeUserInfo is null "+e);
			throw new Exception("Record has not been inserted");
		}
		
		if(responeAdManagementDetail == null)
		{
			System.out.print("responeUserInfo is null");
			throw new Exception("Record has not been inserted");
			
		}
		return responeAdManagementDetail;
	}
	
	@PutMapping("/update")
	@CrossOrigin(origins = "*")
	public AdManagementDetails updateAdManagementDetails(@RequestBody AdManagementDetails requestAdManagementDetails) throws Exception 
	{
		AdManagementDetails responeAdManagementDetail = null;
		LampUtils lampUtils = new LampUtils();
		System.out.println("requestAdManagementDetails :"+requestAdManagementDetails);
		
		if(requestAdManagementDetails !=null)
		{
			//Find By ID
			AdManagementDetails dbAdManagementDetails =   service.fetchById(requestAdManagementDetails.getId()).get();System.out.println("dbAdManagementDetails"+dbAdManagementDetails.getAdvertId());
			if(requestAdManagementDetails.getClientFileName() == null || requestAdManagementDetails.getClientFileName().isEmpty())
			{
				requestAdManagementDetails.setClientFileName(dbAdManagementDetails.getClientFileName());
			}
			
			requestAdManagementDetails.setSubmittedDate(dbAdManagementDetails.getSubmittedDate());
			requestAdManagementDetails.setCreatedDate(dbAdManagementDetails.getCreatedDate());
			requestAdManagementDetails.setModifiedDate(new Date());
			
			requestAdManagementDetails.setAdvertId(requestAdManagementDetails.getAdvertId().trim());
			requestAdManagementDetails.setAdvertTitle(requestAdManagementDetails.getAdvertTitle().trim());
			requestAdManagementDetails.setIssueId(requestAdManagementDetails.getIssueId().trim());
			
			String journalId = requestAdManagementDetails.getJournalName().split(" - ")[0];
			requestAdManagementDetails.setJournalid(journalId);
			
			if(!dbAdManagementDetails.getAdvertId().equals(requestAdManagementDetails.getAdvertId()) 
					|| !dbAdManagementDetails.getAdvertTitle().equals(requestAdManagementDetails.getAdvertTitle())
							|| !dbAdManagementDetails.getColor_mode().equals(requestAdManagementDetails.getColor_mode())
									|| !dbAdManagementDetails.getAdvertSize().equals(requestAdManagementDetails.getAdvertSize())
											||!dbAdManagementDetails.getClientFileName().equals(requestAdManagementDetails.getClientFileName())
												||!dbAdManagementDetails.getJournalid().equals(requestAdManagementDetails.getJournalid()))
			{
				System.out.println("Enter Generate File");
				String ldlFileName = lampUtils.generateLDLFileFromClientFile(requestAdManagementDetails,dbAdManagementDetails.getClientFileName(),dbAdManagementDetails.getJournalid(),true);
				requestAdManagementDetails.setLdlFileName(ldlFileName);
			}
			else
			{
				requestAdManagementDetails.setLdlFileName(dbAdManagementDetails.getLdlFileName());
			}
			
			responeAdManagementDetail = service.saveAdDetails(requestAdManagementDetails);
		}
		
		if(responeAdManagementDetail == null)
		{
			System.out.print("responeUserInfo is null");
			throw new Exception("Record has not been updated");
			
		}
		return responeAdManagementDetail;
	}
	
	
	@PostMapping("/search")
	@CrossOrigin(origins = "*")
	public List<AdManagementDetails> fetchAdDetailsBySearchCriteria(@RequestBody SearchRequest searchRequest) throws Exception 
	{
		List<AdManagementDetails> adManagementDetailList = new ArrayList<AdManagementDetails>();
		if(searchRequest !=null)
		{
			List<String> journalIdList = new ArrayList<String>();
			JournalDetails[] journalIdArray = searchRequest.getJournalId();
			
			if(journalIdArray !=null && journalIdArray.length > 0)
			{
				System.out.println(journalIdArray.length);
				String[] journalIds= new String[journalIdArray.length];
				for(int i=0;i<journalIdArray.length;i++)
				{
					System.out.println(journalIdArray[i].getJournalId());
					journalIds[i]=journalIdArray[i].getJournalId();
				}
				journalIdList = Arrays.asList(journalIds);
			}
			
			String advertId = (searchRequest.getAdvertId()!=null)? searchRequest.getAdvertId():"";
			String issueIdId = (searchRequest.getIssueId()!=null)? searchRequest.getIssueId():"";
			Integer year = (searchRequest.getYear()!=null)? searchRequest.getYear():0;
			String fileName = (searchRequest.getFileName()!=null)? searchRequest.getFileName():"";
			adManagementDetailList = service.fetchAdDetailsBySearchRequest(journalIdList,advertId,issueIdId,year,fileName);
			System.out.println("Ad Management List :"+adManagementDetailList.toString());
		}
		
		if(adManagementDetailList == null || adManagementDetailList.size() <= 0)
		{
			System.out.print("No Record Found");
			//throw new Exception("No Record Found");
			
		}
		
		return adManagementDetailList;
	}
	
	
	@PostMapping("/fetchjournaldetails")
	@CrossOrigin(origins = "*")
	public List<JournalDetails> fetchJournalDetailsByPublisherId(@RequestParam(value = "publisherid") String publisherid) throws Exception 
	{
		List<JournalInfo> journalInfolList = new ArrayList<JournalInfo>();
		List<JournalDetails> journaDetailsList = new ArrayList<JournalDetails>();
		
		if(publisherid !=null)
		{
			System.out.println("publisherid :" +publisherid);
			journalInfolList = service.fetchJournalDetailsByPublisherId(publisherid);
			System.out.println("journalDetailList :"+journalInfolList.toString());
		}
		
		if(journalInfolList == null || journalInfolList.size() <= 0)
		{
			System.out.print("journalDetailList is null");
			throw new Exception("No Record Found");
			
		}
		else
		{
			for(JournalInfo journalInfo : journalInfolList)
			{
				JournalDetails journalDetails = new JournalDetails();
				journalDetails.setJournalTitle(journalInfo.getJournalinfoidentity().getJournalid() +" - "+journalInfo.getJournalTitle());
				journalDetails.setJournalId(journalInfo.getJournalinfoidentity().getJournalid());
				journaDetailsList.add(journalDetails);
			}
		}
		
		return journaDetailsList;
	}
	// active 
	@PutMapping("/updateActiveStatus")
	@CrossOrigin(origins = "*")
	public AdManagementDetails updateActiveStatus(@RequestBody AdManagementDetails adManagementDetails) throws Exception 
	{
		AdManagementDetails responeAdManagementDetail = null;
		List<AdManagementDetails> adManagementDetailList = new ArrayList<AdManagementDetails>(); 
		try {
		if(adManagementDetails !=null)
		{	
			List<String> journalIdList = new ArrayList<String>();
			
			
			String advertId = adManagementDetails.getAdvertId();
		
			adManagementDetailList = service.findDetailsByAdvertId(advertId);System.out.println("adlist"+adManagementDetailList.size());
			for(AdManagementDetails adDetails: adManagementDetailList) {
				String currentStatus = adDetails.getActiveStatus();
				if(currentStatus.equalsIgnoreCase("Active")) {
					adDetails.setActiveStatus("Inactive");
				}
				if(currentStatus.equalsIgnoreCase("Inactive")) {
					throw new Exception("This Advert is already Inactive");
				}
				
				responeAdManagementDetail = service.saveAdDetails(adDetails);
			}
//			String currentStatus = adManagementDetails.getActiveStatus();
//			if(currentStatus.equalsIgnoreCase("Active")) {
//				adManagementDetails.setActiveStatus("Inactive");
//			}
			
		}
		}
        catch(Exception e)
		{
        	e.printStackTrace();
        	System.out.print("responeUserInfo is null "+e);
			throw new Exception("Record has not been inserted");
		}
		
//		if(responeAdManagementDetail == null)
//		{
//			System.out.print("responeUserInfo is null");
//			throw new Exception("Record has not been inserted");
//			
//		}
		return responeAdManagementDetail;
	}
	
	@PutMapping("/getNoOfActiveRecords")
	@CrossOrigin(origins = "*")
	public int getNoOfActiveRecords(@RequestBody AdManagementDetails adManagementDetails) throws Exception 
	{
		AdManagementDetails responeAdManagementDetail = null;
		List<AdManagementDetails> adManagementDetailList = new ArrayList<AdManagementDetails>(); 
		try {
		if(adManagementDetails !=null)
		{	
			List<String> journalIdList = new ArrayList<String>();
			
			
			String advertId = adManagementDetails.getAdvertId();
		
			adManagementDetailList = service.findDetailsByAdvertId(advertId);System.out.println("adlist"+adManagementDetailList.size());
			
//			String currentStatus = adManagementDetails.getActiveStatus();
//			if(currentStatus.equalsIgnoreCase("Active")) {
//				adManagementDetails.setActiveStatus("Inactive");
//			}
			
		}
		}
        catch(Exception e)
		{
        	e.printStackTrace();
        	System.out.print("responeUserInfo is null "+e);
			throw new Exception("Record has not been inserted");
		}
		
//		if(responeAdManagementDetail == null)
//		{
//			System.out.print("responeUserInfo is null");
//			throw new Exception("Record has not been inserted");
//			
//		}
		return adManagementDetailList.size();
	}
	
	@PostMapping("/fetchsearchyears")
	@CrossOrigin(origins = "*")
	public Set<Integer> fetchSearchYears() throws Exception 
	{
		Set<Integer> yearList = new TreeSet<Integer>();

		List<AdManagementDetails> adManagementDetailList = new ArrayList<AdManagementDetails>(); 

		adManagementDetailList = service.fetchAll();
		System.out.println("journalDetailList :"+yearList.toString());
	
		
		if(adManagementDetailList == null || adManagementDetailList.size() <= 0)
		{
			System.out.print("journalDetailList is null");	
		}
		else
		{
			for(AdManagementDetails adDetails : adManagementDetailList)
			{
				yearList.add(adDetails.getYear());
			}
		}
		
		return yearList;
	}
	
	@PostMapping("/fetchyears")
	@CrossOrigin(origins = "*")
	public Set<Integer> fetchYears() throws Exception 
	{
		Set<Integer> yearList = new TreeSet<Integer>();

		int year = Calendar.getInstance().get(Calendar.YEAR);
		
		List<AdManagementDetails> adManagementDetailList = new ArrayList<AdManagementDetails>(); 

		yearList.add(year-1);
		yearList.add(year);
		yearList.add(year+1);
		
		adManagementDetailList = service.fetchAll();
		System.out.println("journalDetailList :"+yearList.toString());
	
		if(adManagementDetailList == null || adManagementDetailList.size() <= 0)
		{
			System.out.print("journalDetailList is null");
			
		}
		else
		{
			for(AdManagementDetails adDetails : adManagementDetailList)
			{
				yearList.add(adDetails.getYear());
			}
		}

		return yearList;
	}
	
	@PostMapping("/fetchconfig")
	@CrossOrigin(origins = "*")
	public List<String> fetchConfigByPublisherIdAndField(@RequestParam(value = "publisherid") String publisherid, @RequestParam(value = "field") String field) throws Exception 
	{
		List<String> configList = new ArrayList<String>();
		
		if(publisherid !=null && field !=null)
		{
			System.out.println("publisherid :" +publisherid);
			System.out.println("field :" +field);
			List<AdvertConfig> advertConfigList = service.fetchAdvertConfig(publisherid,field);
			
		    if(advertConfigList!=null && !advertConfigList.isEmpty())
		    {
		    	for(AdvertConfig advertConfig : advertConfigList)
		    	{
		    		configList.add(advertConfig.getValue());
		    	}
		    }
			
		}

		return configList;
	}
	
	
	@PostMapping("/upload")
	@CrossOrigin(origins = "*")
	public void upload(@RequestBody MultipartFile clientFileName) throws Exception 
	{
		System.out.println("Entered");
		File rootDir = new File("D:\\server\\tomcat8\\webapps\\ROOT\\stockage\\formattertypes\\advert\\APA");
		if(!rootDir.exists())
		{
			boolean result = rootDir.mkdirs();
			System.out.println("rootDir created "+result);
		}
		System.out.println("rootDir "+rootDir);
		File destFile = new File(rootDir+"\\"+clientFileName.getOriginalFilename());
		
		OutputStream os = null;
		try  {
			os = new FileOutputStream(destFile);
		    os.write(clientFileName.getBytes());
		    System.out.println("Uploaded --- ");
		}
        catch (Exception e) {
			System.out.println(e);
		}
        finally {
        	if(os !=null)
        	{
			os.close();
			clientFileName.getInputStream().close();
        	}
		}
	}
	
	@GetMapping("/download")
	@CrossOrigin(origins = "*")
	public void download(String fileName,String journalId, HttpServletResponse response) throws Exception 
	{
		System.out.println("FileName :"+fileName);
		System.out.println("journalId :"+journalId);
		response.setHeader("Content-Disposition", "attachment; filename="+fileName);
		System.out.println("sec");
		response.getOutputStream().write(contentOf(fileName,journalId));
	
	}
	
	private byte[] contentOf(String fileName,String journalId) throws Exception
	{
		System.out.println("contentOf "+fileName);
		Path path = Paths.get("D:\\server\\tomcat8\\webapps\\ROOT\\stockage\\formattertypes\\advert\\APA\\"+journalId+"\\").resolve(fileName);
		System.out.println("path : "+path);
		return Files.readAllBytes(path);
	}
	
}
