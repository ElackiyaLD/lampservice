package com.lumina.lamp.app.lampservice.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(schema = "dbo" , name ="ldl_mst_advert_config")
public class AdvertConfig {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private long id;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "publisherId", nullable = false)
	private String publisherid;

	@Column(name = "field", nullable = false)
	private String field;

	@Column(name = "value", nullable = false)
	private String value;

	// Constructor with fields
	public AdvertConfig(long id,String publisherId, String field, String value) {
		super();
		this.id = id;
		this.publisherid = publisherId;
		this.field = field;
		this.value = value;

	}

	// Empty Constructor
	public AdvertConfig() {

	}

	public String getPublisherid() {
		return publisherid;
	}

	public void setPublisherid(String publisherid) {
		this.publisherid = publisherid;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
