package com.lumina.lamp.app.lampservice.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lumina.lamp.app.lampservice.model.AdManagementDetails;



@Repository
public interface AdManagementRepository extends JpaRepository<AdManagementDetails, Long> {

	public List<AdManagementDetails> findByJournalidInAndAdvertIdContainsAndIssueIdContainsAndClientFileNameContainsOrderByCreatedDateDesc
	(List<String>journalIdList, String advertId,String issueId,String clientFileName);
	
	public List<AdManagementDetails> findByJournalidInAndAdvertIdContainsAndIssueIdContainsAndYearAndClientFileNameContainsOrderByCreatedDateDesc
	(List<String>journalIdList, String advertId,String issueId,Integer year,String clientFileName);
	
	public List<AdManagementDetails> findByAdvertIdContainsAndIssueIdContainsAndClientFileNameContainsOrderByCreatedDateDesc
	(String advertId,String issueId,String clientFileName);
	
	public List<AdManagementDetails> findByAdvertIdContainsAndIssueIdContainsAndYearAndClientFileNameContainsOrderByCreatedDateDesc
	(String advertId,String issueId,Integer year,String clientFileName);
	
	public List<AdManagementDetails> findByAdvertId	(String advertId);
	

}
