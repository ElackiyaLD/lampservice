package com.lumina.lamp.app.lampservice.model;

public class JournalDetails {

	String journalId;
	String journalTitle;

	public JournalDetails() {
		// TODO Auto-generated constructor stub
	}
	
	public String getJournalId() {
		return journalId;
	}

	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}

	public String getJournalTitle() {
		return journalTitle;
	}

	public void setJournalTitle(String journalTitle) {
		this.journalTitle = journalTitle;
	}

}
