package com.lumina.lamp.app.lampservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lumina.lamp.app.lampservice.model.AdManagementDetails;
import com.lumina.lamp.app.lampservice.model.AdvertConfig;
import com.lumina.lamp.app.lampservice.model.JournalInfo;
import com.lumina.lamp.app.lampservice.repository.AdManagementRepository;
import com.lumina.lamp.app.lampservice.repository.AdvertConfigRepository;
import com.lumina.lamp.app.lampservice.repository.JournalInfoRepository;

@Service
public class AdmanagementService {

	@Autowired
	private AdManagementRepository amd_repo;
	
	@Autowired
	private JournalInfoRepository ji_repo;
	
	@Autowired
	private AdvertConfigRepository ac_repo;
	
	public AdManagementDetails saveAdDetails(AdManagementDetails adManagementDetails)
	{
		return amd_repo.save(adManagementDetails);
	}
	
	
//	public AdManagementDetails updateAdDetails(AdManagementDetails adManagementDetails)
//	{
//		return amd_repo.update(adManagementDetails)
//	}
	
	public List<AdManagementDetails> fetchAdDetailsBySearchRequest(List<String> journalIdList, String advertId, 
			String issueId,Integer year, String fileName)
	{

	    boolean isJournalIdEmpty = (journalIdList == null || journalIdList.isEmpty());
	    boolean isYearEmpty = (year == null || year == 0);
	    		
		if(isJournalIdEmpty && isYearEmpty)
		{
			return amd_repo.findByAdvertIdContainsAndIssueIdContainsAndClientFileNameContainsOrderByCreatedDateDesc(advertId, issueId, fileName);
		}
		else if(isYearEmpty)
		{
			return amd_repo.findByJournalidInAndAdvertIdContainsAndIssueIdContainsAndClientFileNameContainsOrderByCreatedDateDesc(journalIdList, advertId, issueId, fileName);
		}
		else if(isJournalIdEmpty)
		{
			return amd_repo.findByAdvertIdContainsAndIssueIdContainsAndYearAndClientFileNameContainsOrderByCreatedDateDesc(advertId, issueId,year, fileName);
		}
		else
		{
			return amd_repo.findByJournalidInAndAdvertIdContainsAndIssueIdContainsAndYearAndClientFileNameContainsOrderByCreatedDateDesc(journalIdList, advertId, issueId,year, fileName);
		}

	}
	
	public List<AdManagementDetails> findDetailsByAdvertId(String advertId) {
		return amd_repo.findByAdvertId(advertId);
	}
	
	public List<JournalInfo> fetchJournalDetailsByPublisherId(String publisher_id)
	{
		return ji_repo.findByjournalinfoidentityPublisherid(publisher_id);
	}
	
	public List<AdManagementDetails> fetchAll()
	{
		return amd_repo.findAll();
	}
	
	public Optional<AdManagementDetails>  fetchById(Long id)
	{
		return amd_repo.findById(id);
	}
	
	public List<AdvertConfig> fetchAdvertConfig(String publisher_id,String field)
	{
		return ac_repo.findByPublisheridAndField(publisher_id, field);
	}
	
}
