package com.lumina.lamp.app.lampservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LampserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LampserviceApplication.class, args);
	}

}
