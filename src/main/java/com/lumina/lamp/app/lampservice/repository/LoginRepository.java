package com.lumina.lamp.app.lampservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lumina.lamp.app.lampservice.model.UserInfo;

@Repository
public interface LoginRepository extends JpaRepository<UserInfo, String>{

	public UserInfo findByUserIdAndPassword(String userID, String password);
}
