package com.lumina.lamp.app.lampservice.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(schema = "dbo" , name ="ldl_mst_ad_management_details")
public class AdManagementDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id",nullable = false)
	private long id;
	
	@Column(name="advertId",nullable = false)
	private String advertId;
	
	@Column(name="advertTitle",nullable = false)
	private String advertTitle;
	
	@Column(name="journalId",nullable = false)
	private String journalid;
	
	@Column(name="journalName",nullable = false)
	private String journalName;
	
	@Column(name="issueId",nullable = false)
	private String issueId;
	
	@Column(name="year",nullable = false)
	private Integer year;
	
	@Column(name="colorMode",nullable = false)
	private String color_mode;
	
	@Column(name="advertSize",nullable = false)
	private String advertSize;
	
	@Column(name="category",nullable = false)
	private String category;
	
	@Column(name="submittedDate",nullable = false)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date submittedDate;
	
	@Column(name="createdDate",columnDefinition = "default 'getDate()'")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date createdDate;

	@Column(name="modifiedDate",columnDefinition = "default 'getDate()'")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date modifiedDate;
	
	@Column(name="clientFileName",nullable = false)
	private String clientFileName;
	
	@Column(name="ldlFileName",nullable = false)
	private String ldlFileName;
	
	@Column(name="activeStatus",nullable = true)
	private String activeStatus;
	
	
	//Constructor with fields
		public AdManagementDetails(String advert_id, String advert_title, String journal_id, 
				String journal_name, String issue_id, Integer year, String color_mode, 
				String advert_size, String category, Date submitted_date,Date createdDate,Date modifiedDate, String client_file_name, 
				String ldl_file_name) 
		{
				super();
				this.advertId=advert_id;
				this.advertTitle=advert_title;
				this.journalid=journal_id;
				this.journalName=journal_name;
				this.issueId=issue_id;
				this.year=year;
				this.color_mode=color_mode;
				this.advertSize=advert_size;
				this.category=category;
				this.submittedDate=submitted_date;
				this.createdDate=createdDate;
				this.modifiedDate=modifiedDate;
				this.clientFileName=client_file_name;
				this.ldlFileName=ldl_file_name;
				//this.activeStatus = activeStatus;
		}
	
	
	public String getActiveStatus() {
			return activeStatus;
		}


		public void setActiveStatus(String activeStatus) {
			this.activeStatus = activeStatus;
		}


	//Empty Constructor
	public AdManagementDetails() {
			
	}
	
	

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getAdvertId() {
		return advertId;
	}

	public void setAdvertId(String advertId) {
		this.advertId = advertId;
	}

	public String getAdvertTitle() {
		return advertTitle;
	}

	public void setAdvertTitle(String advertTitle) {
		this.advertTitle = advertTitle;
	}


	public String getJournalid() {
		return journalid;
	}

	public void setJournalid(String journalid) {
		this.journalid = journalid;
	}

	public String getJournalName() {
		return journalName;
	}

	public void setJournalName(String journalName) {
		this.journalName = journalName;
	}

	public String getIssueId() {
		return issueId;
	}

	public void setIssueId(String issueId) {
		this.issueId = issueId;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getColor_mode() {
		return color_mode;
	}

	public void setColor_mode(String color_mode) {
		this.color_mode = color_mode;
	}

	public String getAdvertSize() {
		return advertSize;
	}

	public void setAdvertSize(String advertSize) {
		this.advertSize = advertSize;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}

	public String getClientFileName() {
		return clientFileName;
	}

	public void setClientFileName(String clientFileName) {
		this.clientFileName = clientFileName;
	}

	public String getLdlFileName() {
		return ldlFileName;
	}

	public void setLdlFileName(String ldlFileName) {
		this.ldlFileName = ldlFileName;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	public Date getModifiedDate() {
		return modifiedDate;
	}


	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
		
}
