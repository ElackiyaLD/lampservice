package com.lumina.lamp.app.lampservice.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import javax.persistence.Table;

@Entity
@Table(schema = "dbo", name = "ldl_mst_journalInfo_CAPS")
public class JournalInfo {

	@EmbeddedId
	private JournalInfoIdentity journalinfoidentity;

	@Column(name="JOURNALTITLE")
	private String journalTitle;

	// Empty Constructor
	public JournalInfo() {

	}

	// Constructor with fields
	public JournalInfo(JournalInfoIdentity journalinfoidentity,String journalTitle) {
		super();
		this.journalinfoidentity = journalinfoidentity;
		this.journalTitle=journalTitle;
	}

	public JournalInfoIdentity getJournalinfoidentity() {
		return journalinfoidentity;
	}

	public void setJournalinfoidentity(JournalInfoIdentity journalinfoidentity) {
		this.journalinfoidentity = journalinfoidentity;
	}

	public String getJournalTitle() {
		return journalTitle;
	}

	public void setJournalTitle(String journalTitle) {
		this.journalTitle = journalTitle;
	}
	
	
}
