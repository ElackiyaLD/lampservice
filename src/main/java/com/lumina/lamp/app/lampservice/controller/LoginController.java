package com.lumina.lamp.app.lampservice.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lumina.lamp.app.lampservice.model.UserInfo;
import com.lumina.lamp.app.lampservice.service.LoginService;
import com.lumina.lamp.app.lampservice.util.TrippleDes;



@RestController
public class LoginController {

	@Autowired
    private LoginService service;
    
	
	@PostMapping("/login")	
	@CrossOrigin(origins = "*")
	public UserInfo loginUser(@RequestBody UserInfo userInfo) throws Exception 
	{
//		String request_emailId = "";
		String request_userId = userInfo.getUserId();
		String request_password = userInfo.getPassword();
//		System.out.print("emailId "+request_emailId);
		TrippleDes trippleDes = new TrippleDes();
		
		UserInfo responeUserInfo= null;
		
		if(request_userId !=null && request_password !=null)
		{
			
			String enctyptedPassword = trippleDes.encrypt(request_password);
			System.out.print("encrypted password "+enctyptedPassword);
			responeUserInfo = service.fetchUserByEmailAndPassword(request_userId,enctyptedPassword);

		}
		
		if(responeUserInfo == null || !("Y").equalsIgnoreCase(responeUserInfo.getLampAccess()) 
				|| !("active").equalsIgnoreCase(responeUserInfo.getUserStatus()))
		{
			System.out.print("responeUserInfo is null");
			throw new Exception("Invalid user credentials");	
		}
		
		return responeUserInfo;
	}
	
}
