package com.lumina.lamp.app.lampservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "dbo",name="ldl_mst_UserInfo") 
public class UserInfo {

	@Id
	@Column(name="USERID",nullable = false)
	private String userId;
	
	@Column(name="USERNAME")
	private String userName;
	
	@Column(name="ROLEID")
	private String roleId;
	
	@Column(name="PASSWORD")
	private String password;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="lamp_access")
	private String lampAccess;
	
	@Column(name="userstatus")
	private String userStatus;
	
	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	//Empty Constructor
	public UserInfo() {
		
	}
	
	//Constructor with fields
	public UserInfo(String userId, String userName, String roleId, String password,String email,String lampAccess,String userStatus) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.roleId = roleId;
		this.password = password;
		this.email = email;
		this.lampAccess=lampAccess;
		this.userStatus=userStatus;
	}
	
	

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getLampAccess() {
		return lampAccess;
	}

	public void setLampAccess(String lampAccess) {
		this.lampAccess = lampAccess;
	}

}
