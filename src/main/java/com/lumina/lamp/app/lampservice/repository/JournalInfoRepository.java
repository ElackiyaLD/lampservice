package com.lumina.lamp.app.lampservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lumina.lamp.app.lampservice.model.JournalInfo;
import com.lumina.lamp.app.lampservice.model.JournalInfoIdentity;

public interface JournalInfoRepository extends JpaRepository<JournalInfo, JournalInfoIdentity>{

	public List<JournalInfo> findByjournalinfoidentityPublisherid(String publisher_id);
}
