package com.lumina.lamp.app.lampservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lumina.lamp.app.lampservice.model.AdvertConfig;

public interface AdvertConfigRepository extends JpaRepository<AdvertConfig,Long> {


	public List<AdvertConfig> findByPublisheridAndField(String publisher_id,String field);

}
