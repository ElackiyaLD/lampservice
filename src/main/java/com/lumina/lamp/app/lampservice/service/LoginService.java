package com.lumina.lamp.app.lampservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lumina.lamp.app.lampservice.model.UserInfo;
import com.lumina.lamp.app.lampservice.repository.LoginRepository;

@Service
public class LoginService {

	@Autowired
	private LoginRepository repo;
	
	
	public UserInfo fetchUserByEmailAndPassword(String userId,String password)
	{
		return repo.findByUserIdAndPassword(userId, password);
	}
}
