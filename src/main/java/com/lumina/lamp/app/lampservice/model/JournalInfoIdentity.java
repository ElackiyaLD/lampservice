package com.lumina.lamp.app.lampservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class JournalInfoIdentity implements Serializable {

	
	@Column(name = "PUBLISHERID", nullable = false)
	private String publisherid;

	@Column(name = "JOURNALID", nullable = false)
	private String journalid;

	// Empty Constructor
	public JournalInfoIdentity() {

	}

	// Constructor with fields
	public JournalInfoIdentity(String publisherid, String journalid) {
		super();
		this.publisherid = publisherid;
		this.journalid = journalid;
	}

	public String getPublisherid() {
		return publisherid;
	}

	public void setPublisherid(String publisherid) {
		this.publisherid = publisherid;
	}

	public String getJournalid() {
		return journalid;
	}

	public void setJournalid(String journalid) {
		this.journalid = journalid;
	}
}
